# README #

This is a client library for communication with a MetCom (Metropolis Communication) Protocol enabled server.

### How do I get set up? ###

If using cmake, clone this repo into your project and include the following lines in your CMakeLists.txt.

    add_subdirectory(libmetcom)
    target_link_libraries(mysolution PRIVATE metcom)

If not using cmake, it may be possible to add the following files to your project:

* include/metcom/metcom.h
* src/metcom.cpp


### Example CMake Project Layout

```bash
# project folder: solution_example
. 
├── CMakeLists.txt
├── main.cpp
└── libmetcom
    ├── CMakeLists.txt
    ├── include
    ├── README.md
    └── src
```

```cmake
# Contents of CMakeLists.txt

cmake_minimum_required(VERSION 3.13)
project(solution_example)

add_executable(solution_example main.cpp)

add_subdirectory(libmetcom)
target_link_libraries(solution_example PRIVATE metcom)
```

```cpp
// Contents of main.cpp

// your code here

int main(){
    // your code here
}
```

### Building a CMake project

Create a a build folder in your project.

    mkdir build

Enter the build folder

    cd build

run cmake on the parent folder (where CMakeLists.txt exists)

    cmake ..

build the project

    cmake --build .
