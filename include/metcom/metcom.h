#ifndef LIBMETCOM_METCOM_H
#define LIBMETCOM_METCOM_H

#include <string>
#include <vector>

#include "metcom_export.h"

namespace metcom {
class MetComClient;
struct MetComChannelData;

class METCOM_EXPORT MetComChannel {
public:
	bool IsOpen();
	bool HasData();
	std::vector<unsigned char> GetData();

	/// MUST CALL CLOSE BEFORE DESTRUCTION
	void Close();

private:
	MetComChannel(unsigned short channel, MetComClient* owner);
	MetComChannelData* _data;

	friend class MetComClient;
};

class METCOM_EXPORT MetComClient {
public:
	MetComClient(const std::string& server, unsigned short port);
	MetComChannel* OpenChannel(unsigned short channel);

private:
	std::vector<unsigned char> _buffer;
};
}  // namespace metcom

#endif