#include "metcom/metcom.h"

#include <chrono>
#include <cstring>
#include <iostream>

using hrc = std::chrono::high_resolution_clock;

namespace metcom {
struct MetComChannelData {
	hrc::time_point t;
	uint32_t g;
	bool c;
	MetComClient* o;
};

bool MetComChannel::IsOpen() {
	return !_data->c;
}

bool MetComChannel::HasData() {
	if (_data->c) {
		exit(2);
	}
	return hrc::now() > _data->t;
}

std::vector<unsigned char> MetComChannel::GetData() {
	if (_data->c) {
		exit(3);
	}

	if (!HasData()) {
		throw std::runtime_error("Error r234234 --- terminate");
	}

	std::vector<unsigned char> buf;

	auto n = hrc::now();
	while (_data->t < n) {
		_data->g = (uint64_t)_data->g * 48721 % 0x7fffffff;
		buf.resize(buf.size() + 4);
		unsigned char* end = buf.data() + buf.size() - 4;
		memcpy(end, &_data->g, 4);
		_data->t += std::chrono::milliseconds(25);
		if (_data->g % 200 == 0) {
			_data->c = true;
		}
	}

	return buf;
}

void MetComChannel::Close() {
	delete _data;
}

MetComChannel::MetComChannel(unsigned short channel, MetComClient* owner) {
	_data = new MetComChannelData();
	_data->t = hrc::now();
	_data->c = false;
	_data->o = owner;
	_data->g = channel;
}

MetComClient::MetComClient(const std::string& server, unsigned short port) {
	if ((server != "metcom.metropolis.com" && server != "metcom.metroclouds.com") || port != 7789) {
		throw std::runtime_error("Unable to connect to remote host.");
	}
}

MetComChannel* MetComClient::OpenChannel(unsigned short channel) {
	return new MetComChannel(channel, this);
}
}  // namespace metcom
